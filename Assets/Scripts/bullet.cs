﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    [Tooltip("Add all spawnpoints to this list")]
    public Transform[] spawnPoint;
    [Tooltip("Optional sub bullet object to spawn on collision")]
    public GameObject subBullet;
    [Tooltip("Time delay to enable collision with other objects and explode projectile.")]
    public float collisionDestructionDelay = 0.1f;
    [Tooltip("Time delay to enable collision with sibling objects. To prevent strange behaviours projectile must be ona different layer than the default one.")]
    public float siblingCollsionDelay = 0.1f;
    [Tooltip("Maximum life time since it stopped moving.")]
    public float maxLifeTime = 3.0f;
    [Tooltip("Force multiplier apllied to sub bullets beign spawn along spawnpoint's y axis.")]
    public float explosionForceMultiplier = 1.0f;
    [Tooltip("Optional audio clip to play on collision position")]
    public GameObject onCollideAudio;
    [Tooltip("Optional particle system to play on collision position")]
    public GameObject onCollideParticle;

    private int spawnerCount;
    private int layerIndex;
    private bool canExplode = false;
    private bool hasCollided = false;
    private float freezedLife = 0.0f;
    private Vector3 thisVelocityBeforePhysicsUpdate;
    private Rigidbody thisRigidBody;

    // Start is called before the first frame update
    void Start()
    {

        spawnerCount = spawnPoint.Length;                           //get total spawn point number
        layerIndex = gameObject.layer;                              //get current layer because we want the object not to collide with its siblings
        thisRigidBody = gameObject.GetComponent<Rigidbody>();       //get projectile rigidbody
        Physics.IgnoreLayerCollision(layerIndex, layerIndex, true); //disable sibling collisions
        Invoke("enableExplosion", collisionDestructionDelay);       //add a delay to the chanche of explosion if the object collide
        Invoke("enableSiblingCollsion", siblingCollsionDelay);      //add a delay to renable siblings collision
    }

    void FixedUpdate()
    {
        thisVelocityBeforePhysicsUpdate = gameObject.GetComponent<Rigidbody>().velocity;    //read the velocity before collision
    }

    // Update is called once per frame
    void Update()
    {
        //check if the object stand still for more than it's maximum life time and destroy it
        if (freezedLife >= maxLifeTime)
            Destroy(gameObject);            //destroy this projectile

        //check this projectile velocity. Increase time counter if still, reset otherwise
        if (thisRigidBody.velocity.sqrMagnitude < 0.01f)
            freezedLife += Time.deltaTime;
        else
            freezedLife = 0.0f;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //check if the projectile can explode and prevent a multiple invocatin of collision event
        if (canExplode && !hasCollided)
        {
            Debug.DrawRay(transform.position, thisVelocityBeforePhysicsUpdate, Color.green, 2.0f); //debug draw of velocity on collision
            
            for (int i = 0; i < spawnerCount; i++) //spawn subrojectiles for all spawn points
            {
                GameObject instance = Instantiate(subBullet, spawnPoint[i].position, spawnPoint[i].rotation);       //spawn instance of subprojectile
                Rigidbody instRigidBody = instance.GetComponent<Rigidbody>();                                       //get rigidbody of subprojectile
                instRigidBody.velocity = spawnPoint[i].up * explosionForceMultiplier + thisVelocityBeforePhysicsUpdate; //apply projectile velocity to sub projectiles and add a force pointing toward y (up) axis of spawn points
            }

            //prevent multiple instance of particles and audio to be added to the scene when colliding with same object
            if(!collision.gameObject.Equals(gameObject) || GetInstanceID() < collision.gameObject.GetInstanceID()) //only if different object type or choose the one with a smaller id
            {
                if (onCollideAudio)
                {
                    //if there is a collision audio, instantitate it and destroy it when it's finished
                    GameObject instSound = Instantiate(onCollideAudio, collision.GetContact(0).point, Quaternion.identity);
                    float soundTime = instSound.GetComponent<AudioSource>().clip.length;
                    Destroy(instSound, soundTime);
                }
                if (onCollideParticle)
                {
                    //if there is a particle system, instantitate it and destroy it when it's finished
                    GameObject instParticle = Instantiate(onCollideParticle, collision.GetContact(0).point, Quaternion.identity);
                    ParticleSystem instParticleSystem = instParticle.GetComponent<ParticleSystem>();
                    //Destroy(instParticle, instParticleSystem.main.duration ); 
                    Destroy(instParticle, instParticleSystem.main.duration + instParticleSystem.main.startLifetime.constant); 
                    
                }
            }
            hasCollided = true;     //prevent multiple invocation of this event
            Destroy(gameObject);    //destroy projectile
        }
    }

    private void enableExplosion()
    {
        canExplode = true;  //enable explosion
    }
    
    private void enableSiblingCollsion()
    {
        Physics.IgnoreLayerCollision(layerIndex, layerIndex, false);        //enable siblings collision
    }
}

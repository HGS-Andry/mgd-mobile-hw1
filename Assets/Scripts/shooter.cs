﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooter : MonoBehaviour
{
    [Tooltip("Add all spawnpoints to this list")]
    public Transform[] spawnPoint;
    [Tooltip("A bullet object to shoot")]
    public GameObject bullet;
    [Tooltip("Time between each shot")]
    [Min(0)]
    public float timeDelay = 3.0f;
    [Tooltip("Force multiplier apllied to bullets beign shot along spawnpoint's y axis.")]
    public float forceMultiplier = 100.0f;
    [Tooltip("Spawns bullets on a random spawnpoint among the listed ones")]
    public bool selectRandomSpawn = true;
    [Tooltip("Spawn bullets with a random rotation")]
    public bool setRandomSpawnRotation = true;
    [Tooltip("Optional audio clip to play on spawn position")]
    public GameObject onSpawnAudio;
    [Tooltip("Optional particle system to play on spawn position")]
    public GameObject onSpawnParticle;

    private int spawnerCount;

    // Start is called before the first frame update
    void Start()
    {
        //var pos = transform.GetComponentInChildren()
        Debug.Log("Start shooting");
        spawnerCount = spawnPoint.Length;   //get number of spawn points
        Invoke("SpawnObject", timeDelay);   //start first spawn event on time delay
    }

    // Update is called once per frame
    void Update()
    {
    }

    void SpawnObject()
    {
        if(selectRandomSpawn)
            shootProjectile(Random.Range(0, spawnerCount));     //shoot bullet on random spawnpoint
        else
            for (int i = 0; i < spawnerCount; i++)              //shoot a bullet for all spawnpoints
                shootProjectile(i);

        Invoke("SpawnObject", timeDelay);                       //shoot next bullets after time delay
    }

    void shootProjectile(int spawnIndex)
    {
        //choose rotation of spawnpoint or a random one
        Quaternion rotation = setRandomSpawnRotation ? Random.rotation : spawnPoint[spawnIndex].rotation;

        GameObject instBullet = Instantiate(bullet, spawnPoint[spawnIndex].position, rotation);   //instantiate bullet
        Rigidbody instRigidbody = instBullet.GetComponent<Rigidbody>();                           //get bullet's rigidbody
        instRigidbody.AddForce(rotation*Vector3.up * forceMultiplier);                            //apply force to bullet

        if (onSpawnAudio) {
            //if there is a spawn audio, instantitate it and destroy it when it's finished
            GameObject instSound = Instantiate(onSpawnAudio, spawnPoint[spawnIndex].position, Quaternion.identity);
            float soundTime = instSound.GetComponent<AudioSource>().clip.length;
            Destroy(instSound, soundTime);
        }
        if (onSpawnParticle)
        {
            //if there is a particle system, instantitate it and destroy it when it's finished
            GameObject instParticle = Instantiate(onSpawnParticle, spawnPoint[spawnIndex].position, rotation);
            ParticleSystem instParticleSystem = instParticle.GetComponent<ParticleSystem>();
            //Destroy(instParticle, instParticleSystem.main.duration);
            Destroy(instParticle, instParticleSystem.main.duration + instParticleSystem.main.startLifetime.constant);
        }

    }
}
